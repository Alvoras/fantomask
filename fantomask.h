#ifndef FANTOMAS_C_INCLUDED
#define FANTOMAS_C_INCLUDED

char* uint_to_b2(int number, int length);
char** addr_split(char* addr);
int getMask(char* addr);
void displayAddr(char* addr, int mask, char* display_addr, char* type);

#endif // FANTOMAS_C_INCLUDED
