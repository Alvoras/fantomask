#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "fantomask.h"

#define USAGE "Usage : %s [-a <valid IP address/CIDR>] [-nb]\n"

int main(int argc, char *argv[]) {
        int opt = 0;
        int i = 0;
        char* addr = NULL;
        int byte_length = 8;
        int addr_length = 32;

        char* type;
        int networkFlag = 0;
        int broadcastFlag = 0;
        int bothFlag = 1;

        if( argc < 3 ) printf(USAGE, argv[0]), exit(EXIT_FAILURE);

        while ( (opt = getopt(argc, argv, "a:nb")) != -1 ) {
                switch (opt) {
                case 'a':
                        addr = optarg;

                        break;
                case 'n':
                        // COMPUTE NETWORK ADDRESS
                        networkFlag = 1;
                        type = "Network";
                        bothFlag = 0;
                        break;
                case 'b':
                        // COMPUTE BROADCAST ADDRESS
                        broadcastFlag = 1;
                        type = "Broadcast";
                        bothFlag = 0;
                        break;
                default:
                        printf(USAGE, argv[0]);
                        exit(EXIT_FAILURE);
                        break;
                }
        }


        char** split = NULL;
        split = addr_split(addr);
        char** addr_b2 = NULL;
        char* addr_b2_full = NULL;
        char* char_mask = strtok(NULL, "/");
        int mask = atoi(char_mask);

        //printf("%s\n", char_mask);

        if (mask > 32) {
                printf("\e[31mERROR\e[1;37m The CIDR mask cannot be over 32 bits (4 bytes)\n");
                exit(EXIT_FAILURE);
        }

        addr_b2 = malloc(sizeof(char*)*4);

        for (i = 0; i < 4; i++) {
                addr_b2[i] = malloc(sizeof(char)*8);
                addr_b2[i] = uint_to_b2(atoi(split[i]), byte_length);
        }

        addr_b2_full = malloc(sizeof(char)*addr_length);

        for (i = 0; i < 4; i++) {
                strcat(addr_b2_full, addr_b2[i]);
        }

        printf("\n");
        printf("    ______          _                            _    \n");
        printf("   |  ____|        | |                          | |   \n");
        printf("   | |__ __ _ _ __ | |_ ___  _ __ ___   __ _ ___| | __\n");
        printf("   |  __/ _` | '_ \\| __/ _ \\| '_ ` _ \\ / _` / __| |/ /\n");
        printf("   | | | (_| | | | | || (_) | | | | | | (_| \\__ \\   < \n");
        printf("   |_|  \\__,_|_| |_|\\__\\___/|_| |_| |_|\\__,_|___/_|\\_\\ v1.1b\n");
        printf("                                                       \n");
        printf("\n");

        if ( (networkFlag) ) {
                // NETWORK ADDRESS
                displayAddr(addr_b2_full, mask, addr, type);
        }else if( (broadcastFlag) ) {
                // BROADCAST ADDRESS
                displayAddr(addr_b2_full, mask, addr, type);
        }else if ( (bothFlag) ) {
                type = "Network";
                displayAddr(addr_b2_full, mask, addr, type);
                printf("\n");
                type = "Broadcast";
                displayAddr(addr_b2_full, mask, addr, type);

        }


        return 0;
}
