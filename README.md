# Fantomask v1.0a
Computes the network and / or the broadcast address from the specified IPv4 address and the specified mask.

Please use this format for the input address :
- IPv4/mask

    Usage : ./fantomask [-a <valid IP address/mask>] [-nb]

-n : Network mode  
- Gives you the network address

-b : Broadcast mode  
- Gives you the broadcast address

If neither of these options are specified, you will get both.
