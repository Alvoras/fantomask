#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

char** addr_split(char* addr){
    char* parsed_addr = strtok(addr, "/");
    char** addr_array = NULL;
    //int addr_length = 12;
    int i = 0;
    int j = 0;
    int k = 0;
    int m = 0;
    int l = 0;

    addr_array = malloc(sizeof(char)*4);

    for (i = 0; i < 4; i++) {
        addr_array[i] = malloc(sizeof(char)*3);
        for (j = 0; j < 3; j++) {
            addr_array[i][j] = '0';
        }
    }

    for (i = 0; i < strlen(parsed_addr)+1; i++) {
        //printf("%c\n", parsed_addr[i]);
        if (parsed_addr[i] == '.' || i > strlen(parsed_addr)-1) {
            l = 0;
            for (j = m; j < i; j++) {
                addr_array[k][j-i+3] = parsed_addr[j];
                l++;
            }
            m = i+1;
            k++;
        }
    }

    return addr_array;
}
char* uint_to_b2(int number, int length) {
    char* b2_number = NULL;
    int i = 0;
    int j = 0;

    b2_number = malloc(sizeof(char)*length);

    for (i = length; i >= 0; i--){
        j = number >> i;

        if (j & 1)
            b2_number[length-i-1] = '1';
        else
            b2_number[length-i-1] = '0';
      }

    return b2_number;
}

int getMask(char* addr){
    char* mask = NULL;

    mask = malloc(sizeof(char)*2);

    strtok(addr, "/");
    mask = strtok(NULL, "/");

    return atoi(mask);
}
void displayAddr(char* addr, int mask, char* dipslay_addr, char* type){
    int i = 0;
    int addr_length = 32;
    char typeValue = '0';
    char addr_b2_full_split[4][9];
    char *addr_string = NULL;
    char buf[5] = {0};

    // 32 bits plus 3 dots plus '\0'
    addr_string = malloc(sizeof(char)*(32+3+1));

    if (strcmp(type, "Broadcast") == 0) {
        typeValue = '1';
    }

    for (i = mask; i < addr_length; i++) {
        addr[i] = typeValue;
    }

    for (i = 0; i < 4; i++) {
            if (i == 3) {
                    strncpy(addr_b2_full_split[3], &addr[23],8);
                    addr_b2_full_split[3][8] = '\0';
            } else {
                    strncpy(addr_b2_full_split[i], &addr[i*8],8);
                    addr_b2_full_split[i][8] = '\0';
            }
    }

    for (i = 0; i < 4; i++) {
            //printf("%ld", strtol(addr_b2_full_split[i], NULL, 2));
            sprintf(buf, "%lu", strtol(&addr_b2_full_split[i], NULL, 2));
            strcat(addr_string, buf);
            if (i < 3) {
                    strcat(addr_string, ".");
            }
    }

    printf("  \e[1;37m%s\e[1;33m %s/%d \e[0m\e[37m\n  ", type, dipslay_addr, mask);

    for (i = 0; i < 4; i++) {
        printf("%.8s", &addr[i*8]);
        if (i < 3) {
            printf(".");
        }else{
            printf("\e[0m");
            printf(" -- %s\n", addr_string);
        }
    }
    printf("      ⁸       ¹⁶       ²⁴       ³²\n");
    printf("\n");
}
